//
//  ViewController.swift
//  Buis_Eye
//
//  Created by PAOLA GUAMANI on 24/4/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var gameSlider: UISlider!
    
    let gameModel = Game()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameModel.restartGame()
        setValues()
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        
        gameModel.play(sliderValue: sliderValue)
        setValues()
        //scoreLabel.text = "\(score)"
        //targetLabel.text = "\(target)"
        //roundLabel.text = "\(roundGame)"
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        gameModel.restartGame()
        setValues()
    }
    
    @IBAction func infoButonPresed(_ sender: Any) {
    }
    
    
    @IBAction func winnerButtonPressed(_ sender: Any) {
        
        if gameModel.isWinner {
            performSegue(withIdentifier: "toWinnerSegue", sender: self)
        }
    }
    
    private func setValues(){
        targetLabel.text = "\(gameModel.target)"
        scoreLabel.text = "\(gameModel.score)"
        roundLabel.text = "\(gameModel.roundGame)"
    }
    
    private func restartGame()  {
        scoreLabel.text = "0"
        targetLabel.text = "\(arc4random_uniform(100))"
        roundLabel.text = "1"
        
    }

    /* code for ... */
    /*@IBAction func changeButtonPressed(_
        sender: UIButton) {
        titleLabel.text = "Hello iOS!"
    }*/
    //si s olo quiero obtener el valor: outlet
}

